/*****************************************************************************
 * Student Name:    Oved Nagar                                               *
 * Id:              302824875                                                *
 * Exercise name:   Ex1                                                      *
 * File description: This file contains the Board Class header               *
 ****************************************************************************/

#ifndef OTHELLO_GAME_H
#define OTHELLO_GAME_H


class Game {
    virtual void startGame() = 0;
};


#endif //OTHELLO_GAME_H
